// The app.
//
// Author: Alexandre Péré
// Date: 04/07/18

/////////////
// Modules //
/////////////
extern crate libexpegit;

/////////////
// Imports //
/////////////
#[macro_use]
extern crate log;
extern crate pretty_logger;
extern crate clap;
use std::env;
use std::fs;
use std::path;

///////////////
// Constants //
///////////////
const NAME: &'static str = env!("CARGO_PKG_NAME");
const VERSION: &'static str = env!("CARGO_PKG_VERSION");
const AUTHOR: &'static str = env!("CARGO_PKG_AUTHORS");
const DESC: &'static str = env!("CARGO_PKG_DESCRIPTION");


/////////
// App //
/////////
fn main(){
    // We initialize the logger.
    pretty_logger::init(pretty_logger::Destination::Stdout,
        log::LogLevelFilter::Debug,
        pretty_logger::Theme::default()).unwrap();

    // We parse the arguments
    let matches = clap::App::new(NAME)
        .version(VERSION)
        .about(DESC)
        .author(AUTHOR)
        .setting(clap::AppSettings::SubcommandRequired)
        .setting(clap::AppSettings::GlobalVersion)
        .subcommand(clap::SubCommand::with_name("init")
            .about("Initializes campaign repository")
            .arg(clap::Arg::with_name("URL")
                .help("Url of the experiment repository")
                .index(1)
                .required(true))
            .arg(clap::Arg::with_name("PATH")
                .help("Path to the local campaign repository")
                .index(2)
                .default_value(".")
                .required(true))
            .arg(clap::Arg::with_name("push")
                .short("p")
                .long("push")
                .help("Pushes the modifications to distant branch")))
        .subcommand(clap::SubCommand::with_name("push")
            .about("Pushes executions to remote"))
        .subcommand(clap::SubCommand::with_name("pull")
            .about("Updates executions from remote"))
        .subcommand(clap::SubCommand::with_name("clone")
            .about("Clones existing campaign repository")
            .arg(clap::Arg::with_name("URL")
                .help("Url of the campaign repository")
                .index(1)
                .required(true))
            .arg(clap::Arg::with_name("PATH")
                .help("Path to clone repository in")
                .index(2)
                .default_value(".")
                .required(true)))
        .subcommand(clap::SubCommand::with_name("exec")
            .about("Manages executions")
            .setting(clap::AppSettings::SubcommandRequired)
            .subcommand(clap::SubCommand::with_name("new")
                .about("Creates a new execution")
                .arg(clap::Arg::with_name("push")
                    .short("p")
                    .long("push")
                    .help("Pushes the modifications to distant branch"))
                .arg(clap::Arg::with_name("COMMIT")
                    .help("Hash value of the commit")
                    .required(true))
                .arg(clap::Arg::with_name("parameters")
                    .help("Script parameters written as they would for the program to start")
                    .multiple(true)
                    .allow_hyphen_values(true)
                    .last(true)))
            .subcommand(clap::SubCommand::with_name("reset")
                .about("Resets an execution")
                .arg(clap::Arg::with_name("IDENTIFIER")
                    .help("Identifier of the execution to reset")
                    .required(true))
                .arg(clap::Arg::with_name("push")
                    .short("p")
                    .long("push")
                    .help("Pushes the modifications to distant branch")))
            .subcommand(clap::SubCommand::with_name("remove")
                .about("Removes an execution")
                .arg(clap::Arg::with_name("IDENTIFIER")
                    .help("Identifier of the execution to remove")
                    .required(true))
                .arg(clap::Arg::with_name("push")
                    .short("p")
                    .long("push")
                    .help("Pushes the modifications to distant branch")))
            .subcommand(clap::SubCommand::with_name("finish")
                .about("Finishes an execution")
                .arg(clap::Arg::with_name("IDENTIFIER")
                    .help("Identifier of the execution to finish")
                    .required(true))
                .arg(clap::Arg::with_name("push")
                    .short("p")
                    .long("push")
                    .help("Pushes the modifications to distant branch"))))
        .get_matches();

    // We check for git/lfs versions
    match libexpegit::misc::check_git_lfs_versions(){
        Err(err) => {
            error!("Git or Lfs is not available to expegit. Is it installed?");
            debug!("Check git version returned: {}", err);
            return;
        },
        Ok(gl) => info!("Git version: {} / Lfs version: {}", gl.0, gl.1),
    };

    // Expegit-Init
    if let Some(matches) = matches.subcommand_matches("init"){
        let url = matches.value_of("URL").unwrap();
        let path = matches.value_of("PATH").unwrap();
        let path = fs::canonicalize(env::current_dir().unwrap().join(path)).unwrap();
        info!("Initializing campaign repository in {} for experiment {}", path.to_str().unwrap(), url);
        match libexpegit::init_cmp_repo(&path, url) {
            Err(err) => {
                error!("An error occurred during initialization: {}", err);
                return;
            }
            Ok(cmp) => {
                info!("Initialization complete: {}", cmp);
            }
        }
        if matches.is_present("push"){
            info!("Pushing changes to remote ... ");
            match libexpegit::push_cmp_repo(&path){
                Err(err) =>{
                    error!("An error occurred while pushing changes to remote: {}", err);
                    return;
                }
                Ok(cmp) => {
                    info!("Push completed: {}", cmp);
                }
            }
        }
        return;
    }

    // Expegit-Clone
    if let Some(matches) = matches.subcommand_matches("clone"){
        match libexpegit::clone_cmp_repo(matches.value_of("URL").unwrap(),
                                      &path::PathBuf::from(matches.value_of("PATH").unwrap())){
            Err(err)=>{
                error!("An error occurred during campaign cloning: {}", err);
                return;
            }
            Ok(cmp)=>{
                info!("Campaign cloned: {}", cmp)
            }
        }
        return;
    }

    // Expegit-Push
   if let Some(matches) = matches.subcommand_matches("push"){
       let cmp_path = match libexpegit::misc::search_expegit_root(&path::PathBuf::from(".")){
           Err(_) => {
               error!("Unable to find an expegit repository in parent folders. Are you in an expegit repository?");
               return ;
           },
           Ok(cmp_path)=> cmp_path,
       };
        match libexpegit::push_cmp_repo(&cmp_path){
            Err(err) =>{
                error!("An error occurred while pushing changes to remote: {}", err);
                return;
            }
            Ok(cmp) => {
                info!("Push completed: {}", cmp);
                return;
            }
        }
   }

    // Expegit-Pull
    if let Some(matches) = matches.subcommand_matches("pull"){
        let cmp_path = match libexpegit::misc::search_expegit_root(&path::PathBuf::from(".")){
            Err(_) => {
                error!("Unable to find an expegit repository in parent folders. Are you in an expegit repository?");
                return ;
            },
            Ok(cmp_path)=> cmp_path,
        };
        match libexpegit::pull_cmp_repo(&cmp_path){
            Err(err)=>{
                error!("An error occurred while pulling changes from remote: {}", err);
                return;
            }
            Ok(cmp)=>{
                info!("Pull Completed: {}", cmp);
                return;
            }
        }
    }

    //Expegit-Exec-New
    if let Some(matches) = matches.subcommand_matches("exec"){
        if let Some(matches) = matches.subcommand_matches("new"){
            let cmp_path = match libexpegit::misc::search_expegit_root(&path::PathBuf::from(".")){
                Err(_) => {
                    error!("Unable to find an expegit repository in parent folders. Are you in an expegit repository?");
                    return ;
                },
                Ok(cmp_path)=> cmp_path,
            };
            let params = match matches.values_of("parameters"){
                None => {
                    info!("No parameters were provided");
                    String::from("")
                },
                Some(params) => {
                    info!("Parameters encountered");
                    params.collect::<Vec<_>>().join(" ")
                },
            };
            match libexpegit::add_exc(&cmp_path,
                                   matches.value_of("COMMIT").unwrap(),
                                   params.as_str()){
                Err(err)=>{
                    error!("An error occurred during execution creation: {}", err);
                    return;
                }
                Ok(exc)=>{
                    info!("Execution initialized: {}", exc)
                }
            }
            if matches.is_present("push"){
                info!("Pushing changes to remote ... ");
                match libexpegit::push_cmp_repo(&cmp_path){
                    Err(err) =>{
                        error!("An error occurred while pushing changes to remote: {}", err);
                        return;
                    }
                    Ok(cmp) => {
                        info!("Push completed: {}", cmp);
                    }
                }
            }
            return;
        }
    }

    // Expegit-Exec-Reset
    if let Some(matches) = matches.subcommand_matches("exec"){
        if let Some(matches) = matches.subcommand_matches("reset"){
            let cmp_path = match libexpegit::misc::search_expegit_root(&path::PathBuf::from(".")){
                Err(_) => {
                    error!("Unable to find an expegit repository in parent folders. Are you in an expegit repository?");
                    return ;
                },
                Ok(cmp_path)=> cmp_path,
            };
            match libexpegit::reset_exc(&cmp_path,
                                   matches.value_of("IDENTIFIER").unwrap()){
                Err(err)=>{
                    error!("An error occurred during execution reset: {}", err);
                    return;
                }
                Ok(exc)=>{
                    info!("Execution reset: {}", exc)
                }
            }
            if matches.is_present("push"){
                info!("Pushing changes to remote ... ");
                match libexpegit::push_cmp_repo(&cmp_path){
                    Err(err) =>{
                        error!("An error occurred while pushing changes to remote: {}", err);
                        return;
                    }
                    Ok(cmp) => {
                        info!("Push completed: {}", cmp);
                    }
                }
            }
            return;
        }
    }

    // Expegit-Exec-Remove
    if let Some(matches) = matches.subcommand_matches("exec"){
        if let Some(matches) = matches.subcommand_matches("remove"){
            let cmp_path = match libexpegit::misc::search_expegit_root(&path::PathBuf::from(".")){
                Err(_) => {
                    error!("Unable to find an expegit repository in parent folders. Are you in an expegit repository?");
                    return ;
                },
                Ok(cmp_path)=> cmp_path,
            };

            match libexpegit::rm_exc(&cmp_path,
                                     matches.value_of("IDENTIFIER").unwrap()){
                Err(err)=>{
                    error!("An error occurred during execution removal: {}", err);
                    return;
                }
                Ok(exc)=>{
                    info!("Execution removed: {}", exc)
                }
            }
            if matches.is_present("push"){
                info!("Pushing changes to remote ... ");
                match libexpegit::push_cmp_repo(&cmp_path){
                    Err(err) =>{
                        error!("An error occurred while pushing changes to remote: {}", err);
                        return;
                    }
                    Ok(cmp) => {
                        info!("Push completed: {}", cmp);
                    }
                }
            }
            return;
        }
    }

    // Expegit-Exec-Finish
    if let Some(matches) = matches.subcommand_matches("exec"){
        if let Some(matches) = matches.subcommand_matches("finish"){
            let cmp_path = match libexpegit::misc::search_expegit_root(&path::PathBuf::from(".")){
                Err(_) => {
                    error!("Unable to find an expegit repository in parent folders. Are you in an expegit repository?");
                    return ;
                },
                Ok(cmp_path)=> cmp_path,
            };
            match libexpegit::finish_exc(&cmp_path,
                                     matches.value_of("IDENTIFIER").unwrap()){
                Err(err)=>{
                    error!("An error occurred during execution finishing: {}", err);
                    return;
                }
                Ok(exc)=>{
                    info!("Execution finished: {}", exc)
                }
            }
            if matches.is_present("push"){
                info!("Pushing changes to remote ... ");
                match libexpegit::push_cmp_repo(&cmp_path){
                    Err(err) =>{
                        error!("An error occurred while pushing changes to remote: {}", err);
                        return;
                    }
                    Ok(cmp) => {
                        info!("Push completed: {}", cmp);
                    }
                }
            }
            return;
        }
    }
}
// This module contains miscellaneous helper functions.
//
// Author: Alexandre Péré
// Date 04/07/18

/////////////
// Imports //
/////////////
extern crate regex;
use std::path;
use std::fs;
use std::process;
use std::io::prelude::*;
use std::str;
use super::Error;
use super::{DATA_RPATH, EXPEGIT_RPATH};

////////////////
// Public API //
////////////////
pub fn write_lfs_gitattributes(path: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Writing lfs gitattributes in {}", path.to_str().unwrap());
    // We append gitattributes file
    let mut gitattributes_file = fs::File::create(path.join(".gitattributes"))?;
    writeln!(gitattributes_file, "# Added by expegit. Do not remove the following line").unwrap();
    writeln!(gitattributes_file, "* filter=lfs diff=lfs merge=lfs -text").unwrap();
    writeln!(gitattributes_file, ".gitattributes -filter -diff -merge text").unwrap();
    gitattributes_file.sync_all()?;
    Ok(())
}

pub fn write_exc_gitignore(path: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Writing executions gitignore in {}", path.to_str().unwrap());
    // We append gitignore file
    let mut gitignore_file = fs::File::create(path.join(".gitignore"))?;
    writeln!(gitignore_file, "# Added by expegit. Do not remove the following lines").unwrap();
    writeln!(gitignore_file, "*").unwrap();
    writeln!(gitignore_file, "!.gitignore").unwrap();
    writeln!(gitignore_file, "!.excconf").unwrap();
    writeln!(gitignore_file, "!{}", DATA_RPATH).unwrap();
    writeln!(gitignore_file, "!{}/*", DATA_RPATH).unwrap();
    writeln!(gitignore_file, "!{}/**/*", DATA_RPATH).unwrap();
    gitignore_file.sync_all()?;
    Ok(())
}

pub fn write_gitkeep(path: &path::PathBuf) -> Result<(), Error>{
    // We log
    info!("Writing executions gitkeep in {}", path.to_str().unwrap());
    // We append gitkeep file
    let mut gitkeep_file = fs::File::create(path.join(".gitkeep"))?;
    writeln!(gitkeep_file, "# Added by expegit. Do not remove.").unwrap();
    gitkeep_file.sync_all()?;
    Ok(())
}

pub fn get_hostname() -> Result<String, Error>{
    // We log
    info!("Retrieving hostname");
    // We retrieve hostname
    let user = str::replace(&String::from_utf8(process::Command::new("id")
        .arg("-u")
        .arg("-n")
        .output()?
        .stdout)
        .unwrap(),"\n", "");
    let host = str::replace(&String::from_utf8(process::Command::new("hostname")
        .output()?
        .stdout)
        .unwrap(), "\n", "");
    return Ok(format!("{}@{}", user, host));
}

pub fn get_all_identifiers(excs_path: &path::PathBuf)-> Result<Vec<String>, Error>{
    // We log
    info!("Getting identifiers");
    // We check the correctness of path
    if !excs_path.exists(){
        panic!("Should provide a valid path.");
    }
    // We get identifiers
    return Ok(fs::read_dir(excs_path)
        .unwrap()
        .into_iter()
        .map(|x| x.unwrap())
        .filter(|x| x.path().is_dir())
        .map(|x| x.path().file_name().unwrap().to_str().unwrap().to_owned())
        .collect());
}

pub fn check_git_lfs_versions()-> Result<(String, String), Error>{
    // We log
    info!("Checking git and lfs versions");
    // We run the version command on git
    let git_version = String::from_utf8(process::Command::new("git")
        .args(&["--version"])
        .output()?.stdout)
        .expect("Failed to parse utf8 string");
    // We run the version command on lfs
    let lfs_version = String::from_utf8(process::Command::new("git")
        .args(&["lfs","--version"])
        .output()?.stdout)
        .expect("Failed to parse utf8 string");
    // We compile the regex to find version in git message.
    let git_regex = regex::Regex::new(r"[0-9]+\.[0-9]+\.[0-9]+")?;
    // We extract the string.
    let git_version = String::from(git_regex.find(&git_version)
        .unwrap()
        .as_str());
    // We compile the regex to find the version in git lfs message
    let lfs_regex = regex::Regex::new(r"[0-9]+\.[0-9]+\.[0-9]+")?;
    // We retrieve the string
    let lfs_version = String::from(lfs_regex.find(&lfs_version)
        .unwrap()
        .as_str());
    // We return the result
    return Ok((git_version, lfs_version));
}

pub fn search_expegit_root(start_path: &path::PathBuf)->Result<path::PathBuf, Error>{
    // We log
    info!("Searching expegit repository root from {}", fs::canonicalize(start_path).unwrap().to_str().unwrap());
    // We loop through parents folders and search expegit file.
    let start_path = fs::canonicalize(start_path)?;
    if start_path.is_file(){panic!("Should provide a folder path.")};
    // We add a dummy folder that will be poped directly to check for .
    let mut start_path = start_path.join("dummy_folder");
    while start_path.pop(){
        if start_path.join(EXPEGIT_RPATH).exists(){
            debug!("Expegit root found at {}", start_path.to_str().unwrap());
            return Ok(start_path);
        }
    }
    debug!("Unable to find .expegit file in parents folders");
    return Err(Error::InvalidRepository);
}

#[cfg(test)]
mod test {
    /////////////
    // Imports //
    /////////////
    use std::fs;
    use std::path;
    use std::collections::HashSet;
    use std::iter::FromIterator;
    use super::*;

    /////////////
    // Statics //
    /////////////
    // The path to tests folders
    static TEST_PATH: &str = "";
    // The host name
    static HOSTNAME: &str = "";

    ///////////
    // Tests //
    ///////////
    #[test]
    fn test_write_lfs_gitattributes(){
        let test_path = path::PathBuf::from(TEST_PATH).join("write_lfs_gitattributes");
        if !test_path.exists(){
            fs::create_dir_all(&test_path).unwrap();
        }
        if test_path.join(".gitattributes").exists() {
            fs::remove_file(test_path.join(".gitattributes")).unwrap();
        }
        write_lfs_gitattributes(&test_path).unwrap();
        assert!(test_path.join(".gitattributes").exists());
    }

    #[test]
    fn test_write_exc_gitignore(){
        let test_path = path::PathBuf::from(TEST_PATH).join("write_exc_gitignore");
        if !test_path.exists(){
            fs::create_dir_all(&test_path).unwrap();
        }
        if test_path.join(".gitignore").exists() {
            fs::remove_file(test_path.join(".gitignore")).unwrap();
        }
        write_exc_gitignore(&test_path).unwrap();
        assert!(test_path.join(".gitignore").exists());
    }

    #[test]
    fn test_write_gitkeep(){
        let test_path = path::PathBuf::from(TEST_PATH).join("write_exc_gitkeep");
        if !test_path.exists(){
            fs::create_dir_all(&test_path).unwrap();
        }
        if test_path.join(".gitkeep").exists() {
            fs::remove_file(test_path.join(".gitkeep")).unwrap();
        }
        write_gitkeep(&test_path).unwrap();
        assert!(test_path.join(".gitkeep").exists());
    }

    #[test]
    fn test_get_hostname(){
        let hostname = get_hostname().unwrap();
        assert_eq!(hostname, HOSTNAME);
    }


    #[test]
    fn test_check_git_lfs_versions() {
        check_git_lfs_versions().unwrap();
    }

    #[test]
    fn test_get_identifiers(){
        let test_path = path::PathBuf::from(TEST_PATH).join("get_all_identifiers");
        if !test_path.exists(){
            fs::create_dir_all(&test_path).unwrap();
            fs::create_dir(&test_path.join("1")).unwrap();
            fs::create_dir(&test_path.join("2")).unwrap();
            fs::create_dir(&test_path.join("3")).unwrap();
        }
        let identifiers = get_all_identifiers(&test_path).unwrap();
        let identifiers: HashSet<String> = HashSet::from_iter(identifiers.iter().map(|x| x.to_owned()));
        let expected_identifiers = vec![String::from("1"),
                                        String::from("2"),
                                        String::from("3")];
        let expected_identifiers: HashSet<String> = HashSet::from_iter(expected_identifiers.iter().map(|x| x.to_owned()));
        assert_eq!(identifiers, expected_identifiers);
    }

    #[test]
    fn test_search_expegit_root(){
        let test_path = path::PathBuf::from(TEST_PATH).join("search_expegit_root");
        if !test_path.exists(){
            fs::create_dir_all(&test_path).unwrap();
            fs::create_dir_all(&test_path.join("no/1/2")).unwrap();
            fs::create_dir_all(&test_path.join("yes/1/2")).unwrap();
            fs::File::create(&test_path.join("yes/.expegit")).unwrap();
        }
        let no_res = search_expegit_root(&test_path.join("no/1/2"));
        assert!(no_res.is_err());
        let yes_res = search_expegit_root(&test_path.join("yes/1/2"));
        assert!(yes_res.is_ok());
        assert_eq!(yes_res.unwrap(), test_path.join("yes"));
        let yes_res = search_expegit_root(&test_path.join("yes"));
        assert!(yes_res.is_ok());
        assert_eq!(yes_res.unwrap(), test_path.join("yes"));
    }
}
